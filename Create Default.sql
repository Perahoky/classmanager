-- Creator:       MySQL Workbench 5.2.45/ExportSQLite plugin 2009.12.02
-- Author:        Pera
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2013-01-19 01:46
-- Created:       2013-01-18 22:27
PRAGMA foreign_keys = OFF;

-- Schema: StudentsDatabase
BEGIN;
CREATE TABLE "asd"(
  "idasd" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL
);
CREATE TABLE "companies"(
  "company_id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(50),
  "address1" VARCHAR(20),
  "address2" VARCHAR(20),
  "postal_code" VARCHAR(10),
  "city" VARCHAR(30),
  "country" VARCHAR(40),
  "studentscount" INTEGER,
  "studentsmarkaverage" DOUBLE
);
CREATE TABLE "classes"(
  "class_id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(10),
  "students" INTEGER,
  "marks_average" DOUBLE
);
CREATE TABLE "genders"(
  "gender_id" INTEGER PRIMARY KEY NOT NULL,
  "title" VARCHAR(10)
);
CREATE TABLE "additionalinformationstypes"(
  "type_id" INTEGER PRIMARY KEY NOT NULL,
  "title" VARCHAR(10)
);
CREATE TABLE "students"(
  "student_id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(40),
  "description" VARCHAR(40),
  "age" INTEGER,
  "class_id" INTEGER,
  "company_id" INTEGER,
  "marksaverage" DOUBLE,
  "gender_id" INTEGER,
  CONSTRAINT "fk_student_class"
    FOREIGN KEY("class_id")
    REFERENCES "classes"("class_id")
    ON DELETE RESTRICT,
  CONSTRAINT "fk_student_conmpany"
    FOREIGN KEY("company_id")
    REFERENCES "companies"("company_id"),
  CONSTRAINT "fk_student_gender"
    FOREIGN KEY("gender_id")
    REFERENCES "genders"("gender_id")
);
CREATE INDEX "students.fk_student_class_idx" ON "students"("class_id");
CREATE INDEX "students.fk_student_conmpany_idx" ON "students"("company_id");
CREATE INDEX "students.fk_student_gender_idx" ON "students"("gender_id");
CREATE TABLE "additionalinformations"(
  "ainfo_id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "student_id" INTEGER,
  "name" VARCHAR(20),
  "value" BLOB,
  "ainfotype_id" INTEGER NOT NULL,
  "descriptions" VARCHAR(200),
  CONSTRAINT "fk_student_id"
    FOREIGN KEY("student_id")
    REFERENCES "students"("student_id"),
  CONSTRAINT "fk_ainfotype_id"
    FOREIGN KEY("ainfotype_id")
    REFERENCES "additionalinformationstypes"("type_id")
);
CREATE INDEX "additionalinformations.fk_student_id_idx" ON "additionalinformations"("student_id");
CREATE INDEX "additionalinformations.fk_ainfotype_id_idx" ON "additionalinformations"("ainfotype_id");
CREATE TABLE "marks"(
  "mark_id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "mark" DOUBLE,
  "description" VARCHAR(200),
  "student_id" INTEGER,
  CONSTRAINT "fk_student_id"
    FOREIGN KEY("student_id")
    REFERENCES "students"("student_id")
);
CREATE INDEX "marks.fk_student_id_idx" ON "marks"("student_id");
COMMIT;
