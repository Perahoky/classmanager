﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ClassManager
{
	static class Program
	{
		/// <summary>
		/// Der Haupteinstiegspunkt für die Anwendung.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			try
			{
				Core.Init();
				Core.Start();
			}
			catch (Exception ex)
			{
				MessageBox.Show("The program had to be terminated due to the following errror:\r\n" + ex.Message,
								"ClassManager | " + ex.GetType().Name);
			}
		}
	}
}
