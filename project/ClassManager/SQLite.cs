﻿/* *** HEADER .\SQlite.cs ***
 *
 * # COPYRIGHT © Paul Holz - PeraSource - 2013
 * # ClassManager source file
 *
 * # Devoloped as homework for Mr. Jorcyk.
 *
 * # LICENSE / RIGHTS / OWNER:
 *		This code file is devoloped by Paul Holz. 
 *		If you use any code from here, please set a reference to this project!
 *
 * # CREATED:		8.1.2013
 * # BY:			Paul Holz
 * # SUMMARY:		SQlite presents the wrapper for accessing the sqlite database.
 * # LAST UPDATE:	21.1.2013
 * # BY:			Paul Holz
 * # SUMMARY:		Completed this version of database-creation.
 * 
 * # CHANGELOG
 *		# 10.1.2013
 *			- Created.
 *		# 21.1.2013:
 *			- Completed Creation-func for database of this version.
 *			- Moved class to static, because functions are unique.
 *				
 * # BUGTRACKER
 * 
 * # TODO
 * 
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace ClassManager
{
	/// <summary>
	/// This class contains every database function which is needed by this program.
	/// That means functions like "create all database tables" or "get value x frpom table z".
	/// This class is understanded like an "interface" from this program to the SQLite-Interface. Program  - this - SQLite
	/// </summary>
	public static class SQLite
	{
		#region Public Properties
		public static string DbLocation
		{
			get { return dbLocation; }
			set { dbLocation = value; }
		}
		public static SQLiteConnection Connection
		{
			get { return connection; }
			set { connection = value; }
		}
		public static SQLiteTransaction DefaultTransaction
		{
			get { return defaultTransaction; }
			set { defaultTransaction = value; }
		}
		public static bool Loaded { get; set; }
		#endregion Public Properties


		#region Private Fields
		private static SQLiteConnection connection;
		private static SQLiteTransaction defaultTransaction;
		private static string dbLocation;
		#endregion Private Fields


		#region CCTOR

		#endregion CCTOR


		#region Public Functions
		public static void Init(string dbLocation)
		{
			SQLite.dbLocation = dbLocation;
			SQLiteConnectionStringBuilder builder = new SQLiteConnectionStringBuilder { DataSource = dbLocation };
			connection = new SQLiteConnection(builder.ConnectionString);
		}

		public static bool Exist()
		{
			return File.Exists(dbLocation);
		}

		/// <summary>
		/// This functions fills the database with her default content.
		/// </summary>
		public static void Create()
		{

			try
			{
				SQLiteConnection.CreateFile(dbLocation);
				SQLiteCommand cmd;
				connection.Open();

				defaultTransaction = connection.BeginTransaction();

				// * Create default tables.
				//using (cmd = connection.CreateCommand())
				//{
				//    cmd.CommandText = SQLFactory.BEGIN;
				//    cmd.Transaction = defaultTransaction;
				//    cmd.ExecuteNonQuery();
				//}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_TBL_COMP;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_TBL_CLASSES;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_TBL_GENDERS;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_TBL_AINFOT;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_TBL_STUDENTS;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_IDX_STUDENTS_FKCLASSID;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_IDX_STUDENTS_FKCOMP;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_IDX_STUDENTS_FKGENDER;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_TBL_AINFO;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_IDX_AINFO_FKSTUDENTID;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_IDX_AINFO_FKINFOTID;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_TBL_MARKS;
					cmd.ExecuteNonQuery();
				}
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.CREATE_IDX_MARKS_STUDENTID;
					cmd.ExecuteNonQuery();
				}
				// *

				// create default values for genders table.
				using (cmd = connection.CreateCommand())
				{
					cmd.Transaction = DefaultTransaction;
					cmd.CommandText = SQLFactory.INSERT_TBL_GENDERS_DEFAULT;
					cmd.ExecuteNonQuery();
				}


				// commit.
				//using (cmd = connection.CreateCommand())
				//{
				//    cmd.Transaction = DefaultTransaction;
				//    cmd.CommandText = SQLFactory.COMMIT;
				//    cmd.ExecuteNonQuery();
				//}
				DefaultTransaction.Commit();
			}
			catch (Exception ex)
			{
				throw new Exception("Error creating database!\r\n" + ex.ToString() + ":\r\n" + ex.Message);
			}
			finally
			{
				DefaultTransaction.Rollback();
			}
			/* old stements
				command = this.connection.CreateCommand();
				command.Transaction = defaultTransaction;
				command.CommandText = SQLFactory.BEGIN;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_TBL_COMP;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_TBL_CLASSES;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_TBL_GENDERS;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_TBL_AINFOT;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_TBL_STUDENTS;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_IDX_STUDENTS_FKCLASSID;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_IDX_STUDENTS_FKCOMP;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_IDX_STUDENTS_FKGENDER;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_TBL_AINFO;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_IDX_AINFO_FKSTUDENTID;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_IDX_AINFO_FKINFOTID;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_TBL_MARKS;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.CREATE_IDX_MARKS_STUDENTID;
				command.ExecuteNonQuery();
				command.CommandText = SQLFactory.COMMIT;
				command.ExecuteNonQuery();
			*/
		}
		#endregion Public Functions

		private static class SQLFactory
		{
			#region Const
			public const string COMMIT = "COMMIT;";
			public const string BEGIN = "BEGIN;";

			/// <summary>
			/// Create Default COMPANY table.
			/// </summary>
			public const string CREATE_TBL_COMP =
				"CREATE TABLE \"companies\" ( \"company_id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \"name\" VARCHAR(50), \"address1\" VARCHAR(20), \"address2\" VARCHAR(20), \"postal_code\" VARCHAR(10), \"city\" VARCHAR(30), \"country\" VARCHAR(40), \"studentscount\" INTEGER, \"studentsmarkaverage\" DOUBLE );";
			/// <summary>
			/// Create default CLASSES table.
			/// </summary>
			public const string CREATE_TBL_CLASSES =
				"CREATE TABLE \"classes\" ( \"class_id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \"name\" VARCHAR(10), \"students\" INTEGER, \"marks_average\" DOUBLE );";
			/// <summary>
			/// Create default GENDERS table.
			/// </summary>
			public const string CREATE_TBL_GENDERS =
				"CREATE TABLE \"genders\" ( \"gender_id\" INTEGER PRIMARY KEY NOT NULL, \"title\" VARCHAR(10) );";
			/// <summary>
			/// Create default ADDITIONALINFORMATIONTYPES table.
			/// </summary>
			public const string CREATE_TBL_AINFOT =
				"CREATE TABLE \"additionalinformationstypes\" ( \"type_id\" INTEGER PRIMARY KEY NOT NULL, \"title\" VARCHAR(10) );";
			/// <summary>
			/// Create default STUDENTS table.
			/// </summary>
			public const string CREATE_TBL_STUDENTS =
				"CREATE TABLE \"students\" (\"student_id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \"name\" VARCHAR(40), \"description\" VARCHAR(40), \"age\" INTEGER, \"class_id\" INTEGER, \"company_id\" INTEGER, \"marksaverage\" DOUBLE, \"gender_id\" INTEGER, CONSTRAINT \"fk_student_class\"   FOREIGN KEY(\"class_id\")   REFERENCES \"classes\"(\"class_id\")   ON DELETE RESTRICT, CONSTRAINT \"fk_student_conmpany\"   FOREIGN KEY(\"company_id\")   REFERENCES \"companies\"(\"company_id\"), CONSTRAINT \"fk_student_gender\"   FOREIGN KEY(\"gender_id\")   REFERENCES \"genders\"(\"gender_id\") );";
			/// <summary>
			/// Create default ADDITIONALINFORMATIONS table.
			/// </summary>
			public const string CREATE_TBL_AINFO =
				"CREATE TABLE \"additionalinformations\"(\"ainfo_id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\"student_id\" INTEGER,\"name\" VARCHAR(20),\"value\" BLOB,\"ainfotype_id\" INTEGER NOT NULL,\"descriptions\" VARCHAR(200),CONSTRAINT \"fk_student_id\"FOREIGN KEY(\"student_id\")REFERENCES \"students\"(\"student_id\"),CONSTRAINT \"fk_ainfotype_id\"FOREIGN KEY(\"ainfotype_id\")REFERENCES \"additionalinformationstypes\"(\"type_id\"));";
			/// <summary>
			/// Create default MARKS table.
			/// </summary>
			public const string CREATE_TBL_MARKS =
				"CREATE TABLE \"marks\" ( \"mark_id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \"mark\" DOUBLE, \"description\" VARCHAR(200), \"student_id\" INTEGER, CONSTRAINT \"fk_student_id\"FOREIGN KEY ( \"student_id\" ) REFERENCES \"students\" ( \"student_id\" ) );";

			/// <summary>
			/// Create STUDENTS-CLASS-ID index.
			/// </summary>
			public const string CREATE_IDX_STUDENTS_FKCLASSID =
				"CREATE INDEX \"students.fk_student_class_idx\" ON \"students\" ( \"class_id\" );";
			/// <summary>
			/// Create STUDENTS-COMPANY-ID index.
			/// </summary>
			public const string CREATE_IDX_STUDENTS_FKCOMP =
				"CREATE INDEX \"students.fk_student_conmpany_idx\" ON \"students\" ( \"company_id\" );";
			/// <summary>
			/// Create STUDENTS-GENDER-ID index.
			/// </summary>
			public const string CREATE_IDX_STUDENTS_FKGENDER =
				"CREATE INDEX \"students.fk_student_gender_idx\" ON \"students\" ( \"gender_id\" );";
			/// <summary>
			/// Create ADDITIONALINFORMATIONS-STUDENTS-ID index.
			/// </summary>
			public const string CREATE_IDX_AINFO_FKSTUDENTID =
				"CREATE INDEX \"additionalinformations.fk_student_id_idx\" ON \"additionalinformations\"(\"student_id\");";
			/// <summary>
			/// Create ADDITIONALINFORMATION-TYPE-ID index.
			/// </summary>
			public const string CREATE_IDX_AINFO_FKINFOTID =
				"CREATE INDEX \"additionalinformations.fk_ainfotype_id_idx\" ON \"additionalinformations\"(\"ainfotype_id\");";
			/// <summary>
			/// Create MARKS-Student-ID index.
			/// </summary>
			public const string CREATE_IDX_MARKS_STUDENTID =
				"CREATE INDEX \"marks.fk_student_id_idx\" ON \"marks\" ( \"student_id\" );";

			/// <summary>
			/// Create GENDERS
			/// </summary>
			public const string INSERT_TBL_GENDERS_DEFAULT =
				"INSERT INTO 'genders' ('gender_id', 'title') VALUES (0, 'Unknown'), (1, 'Male'), (2, 'Female');";
			#endregion Const
		}
	}


	enum DBState
	{
		None = 0,
		New = 1,
		NewModified = 2,
		Notmodified = 3,
		Modified = 4
	}
}
