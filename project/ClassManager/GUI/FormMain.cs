﻿
/* *** HEADER ***
 *
 * # COPYRIGHT © Paul Holz - PeraSource - 2013
 * # ClassManager source file
 *
 * # Devoloped as homework for Mr. Jorcyk.
 * *
 * # CREATED:		10.1.13
 * # BY:			Paul Holz
 * # SUMMARY:		The main window
 * # LAST UPDATE:	10.1.13
 * # BY:			paul holz
 * # SUMMARY:		-
 * 
 * # CHANGELOG
 *		- 10.1.13:
 *			- controls added
 *				
 * # BUGTRACKER
 * 
 * # TODO
 * 
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClassManager
{
	public partial class FormMain : Form
	{
		public FormMain()
		{
			InitializeComponent();
		}
	}
}
