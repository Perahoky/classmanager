﻿/* *** HEADER .\PupilsClass.cs | PupilsClass main file ***
 *
 * # COPYRIGHT © Paul Holz - PeraSource - 2013
 * # ClassManager source file
 *
 * # Devoloped as homework for Mr. Jorcyk.
 *
 * # CREATED:		10.1.13
 * # BY:			paul holz
 * # SUMMARY:		represents a student person with it's properties like age, sex..
 * # LAST UPDATE:	10.1.13
 * # BY:			paul holz
 * # SUMMARY:		-
 * 
 * # CHANGELOG
 *		- 10.1.13:
 *			- cctor added
 *			- properties added
 *			- fields added
 *			- 
 *			
 *		- 14.1.13:
 *			- Added many comments and descriptions.
 *			- Add ContainsStudent(StudentPerson student) function: returns a boolean, true if the student is contained in this class.
 *				
 * # BUGTRACKER
 * 
 * 
 * # TODO
 *	- make impressive!
 * 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassManager.DataBase
{
	// a bit failed naming...
	class StudentsClass
	{
		#region Static's
		static public AdditionalClassInformation[] AdditionalInformationsTemplate { get; set; }
		#endregion Static's

		#region Public Fields
		private string name;
		private List<StudentPerson> students;
		private List<AdditionalClassInformation> additionalInformations;
		#endregion Public Fields

		#region Public properties
		/// <summary>
		/// Get an array of all students in this class.
		/// Changes made to this array take no effect to the intern list!
		/// </summary>
		public StudentPerson[] Students
		{
			get { return students.ToArray(); }
		}
		/// <summary>
		/// The name of this StudentsClass object.
		/// </summary>
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}
		/// <summary>
		/// The additionalinformations of this studentsclass.
		/// </summary>
		public List<AdditionalClassInformation> AdditionalInformations
		{
			get { return this.additionalInformations; }
			set { this.additionalInformations = value; }
		}
		/// <summary>
		/// Get the count of students in this class.
		/// </summary>
		public uint PupilsCount
		{
			get { return (uint)this.students.Count; }
		}
		#endregion Public properties

		/// <summary>
		/// Simple constructor (cctor), only the name of the new class.
		/// </summary>
		/// <param name="name"></param>
		public StudentsClass(string name)
		{
			this.name = name;
		}

		public StudentsClass(string name, IEnumerable<StudentPerson> collection)
			: this(name)
		{
			this.students = new List<StudentPerson>(collection);
			foreach (var studentPerson in this.students)
			{
				studentPerson.SetStudentClass(this);
			}
		}

		/// <summary>
		/// This functions removes an student from this class.
		/// </summary>
		/// <param name="person">The student to remove from this class.</param>
		public void RemoveStudent(StudentPerson person)
		{
			this.students.Remove(person);
		}

		/// <summary>
		/// This function checks whether the given student is contained in this class.
		/// </summary>
		/// <param name="student"></param>
		/// <returns></returns>
		public bool ContainsStudent(StudentPerson student)
		{
			return this.Students.Contains(student);
		}


		#region Struct's
		public struct AdditionalClassInformation
		{
			// DB-Properties



			// the following 3 properties a necessary
			public string ID { get; private set; }
			public object Value { get; private set; }
			public Type Type { get; private set; }

			public string Description { get; set; }

			public AdditionalClassInformation(string id, object value, Type type, string description = "")
				: this()
			{
				this.ID = id;
				this.Value = value;
				this.Type = type;
				this.Description = description;
			}
		}
		#endregion Struct's
	}
}
