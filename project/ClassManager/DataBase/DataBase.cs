﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassManager.DataBase
{
	class DataBase
	{
		public List<StudentPerson> students { get; set; }
		public List<StudentsClass> classes { get; set; }
		public List<Company> companies { get; set; }


	}
}
