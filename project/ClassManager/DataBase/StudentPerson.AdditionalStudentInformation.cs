﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassManager.DataBase
{
	partial class StudentPerson
	{
		/// <summary>
		/// A AdditionalPupilInformation is a little object, which contains informations about a pupil which are not implemented on compile-time.
		/// So, the user can add his own informations to a single student of all.
		/// To add a informations-template to all students, add a instance to the informations-templates-list. 
		/// <code> StudentPerson.AddInformationTemplate(new AdditionalPupilInformation("id", (object)"value", typeof(string), "description")); </code>
		/// </summary>
		public struct AdditionalStudentInformation
		{
			// the following 3 properties a necessary
			/// <summary>
			/// Get the id of this information.
			/// </summary>
			public string ID { get; private set; }
			/// <summary>
			/// Get the value of this ifnromation.
			/// </summary>
			public object Value { get; private set; }
			/// <summary>
			/// Get the Type of this information. (e.g. int or string)
			/// </summary>
			public Type Type { get; private set; }

			/// <summary>
			/// Get or set the description.
			/// </summary>
			public string Description { get; set; }

			/// <summary>
			/// Create a new instance.
			/// Description is a optional parameter, the other's are necessary.
			/// </summary>
			/// <param name="id">The id of new the information.</param>
			/// <param name="value">The value of the new information.</param>
			/// <param name="type">The type of the new information.</param>
			/// <param name="description">(Optional) The description of the new information.</param>
			public AdditionalStudentInformation(string id, object value, Type type, string description = "") : this()
			{
				this.ID = id;
				this.Value = value;
				this.Type = type;
				this.Description = description;
			}

			/// <summary>
			/// Returns a plaintext string of the current instance in the following format:
			/// #ID# (#TYPE#): #VALUE#
			/// </summary>
			/// <returns></returns>
			public override string ToString()
			{
				return ID + " (" + Type.Name + "): " + Value;
			}
		}
	}
}
