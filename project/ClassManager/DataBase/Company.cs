﻿/* *** .\DataBase\Company.cs HEADER ***
 *
 * # COPYRIGHT © Paul Holz - PeraSource - 2013
 * # ClassManager source file
 *
 * # Devoloped as homework for Mr. Jorcyk.
 *
 * # CREATED:		10.1.13
 * # BY:			paul holz
 * # SUMMARY:		represents a company which contains some students and some additional informations.
 * # LAST UPDATE:	21.1.13
 * # BY:			paul holz
 * # SUMMARY:		Added 2 empty constructor :P
 * 
 * # CHANGELOG
 *		- 10.1.13:
 *			- Created
 *			
 * 
 * # BUGTRACKER
 * 
 * 
 * # TODO
 *	- make impressive!
 * 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassManager.DataBase
{
	class Company
	{
		#region Private Fields
		private string name;
		private string address;
		private string postalCode;
		private string city;
		private string country;
		private int studentsCount;
		private double studentsmarksAverage;
		#endregion Private Fields

		#region Public properties
		public string Name
		{
			get { return name; }
			set { name = value; }
		}
		public string Address
		{
			get { return address; }
			set { address = value; }
		}
		public string PostalCode
		{
			get { return postalCode; }
			set { postalCode = value; }
		}
		public string City
		{
			get { return city; }
			set { city = value; }
		}
		public string Country
		{
			get { return country; }
			set { country = value; }
		}
		public int StudentsCount
		{
			get { return studentsCount; }
			set { studentsCount = value; }
		}
		public double StudentsmarksAverage
		{
			get { return studentsmarksAverage; }
			set { studentsmarksAverage = value; }
		}
		#endregion Public properties

		#region CCTOR
		public Company(string name)
		{




		}
		public Company(string name, string address)
		{




		}

		#endregion CCTOR










	}
}
