﻿/* .\StudentPerson.cs HEADER 
 *
 * # COPYRIGHT © Paul Holz - PeraSource - 2013
 * # ClassManager source file
 *
 * # Devoloped as homework for Mr. Jorcyk.
 *
 * # CREATED:		10.1.13
 * # BY:			paul holz
 * # SUMMARY:		represents a student person with it's properties like age, sex..
 * # LAST UPDATE:	10.1.13
 * # BY:			paul holz
 * # SUMMARY:		-
 * 
 * # CHANGELOG
 *		- 10.1.13:
 *			- cctor added
 *			- properties added
 *			- fields added
 *			
 *		- 14.1.13:
 *			- Added many comments and descriptions.
 *			
 *		- 21.1.2013
 *			- Changed AdditionalPupilInformations to AdditionalStudentInformation, just like the class's name.
 *				
 * # BUGTRACKER
 * 
 * 
 * # TODO
 *		- correct object linking. (Student <-> company...)
 * 
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ClassManager.DataBase
{
	partial class StudentPerson
	{
		#region static
		/// <summary>
		/// List-Backfield for a property <see cref="AdditionalInformationTemplates"/>.
		/// </summary>
		private static List<AdditionalStudentInformation> additionalInformationTemplates;

		/// <summary>
		/// Get's an array of additionalinformation's as template for a new created student.
		/// This list is copied when a new student is created.
		/// Changes on this property makes no effect to the intern list!
		/// Use <see cref="AddInformationTemplate"/> or <see cref="RemoveInformationTemplate"/> to add or remove templates!
		/// </summary>
		public static AdditionalStudentInformation[] AdditionalInformationTemplates
		{
			// the getter return only an array, so no changes to the list are possible.
			get { return additionalInformationTemplates.ToArray(); }
		}

		/// <summary>
		/// This little cctor creates the instance for additionalinformatuiontemapltes.
		/// </summary>
		static StudentPerson()
		{
			// make sure that the templates list everytime have a object reference
			additionalInformationTemplates = new List<AdditionalStudentInformation>();
		}

		/// <summary>
		/// This function add's a additionalinformation as template for new created students.
		/// </summary>
		/// <param name="information">A instance of <see cref="AdditionalPupilInformation"/> which will be added to the templates. </param>
		public static void AddInformationTemplate(AdditionalStudentInformation information)
		{
			additionalInformationTemplates.Add(information);
		}

		/// <summary>
		/// This function removes a additionalinformation from the templates.
		/// </summary>
		/// <param name="information">A instance of <see cref="AdditionalPupilInformation"/> which will be removed from the templates list.</param>
		public static void RemoveInformationTemplate(AdditionalStudentInformation information)
		{
			additionalInformationTemplates.Remove(information);
		}
		#endregion static

		#region Private Fields
		private FullName name;
		private StudentsClass studentClass;
		private string description;
		private uint? age;
		private Sex gender;
		private List<AdditionalStudentInformation> additionalInformations;
		#endregion Private Fields

		#region Public Properties
		/// <summary>
		/// Get or set the sex/gender of this student.
		/// </summary>
		public Sex Gender
		{
			get { return this.gender; }
			set { this.gender = value; }
		}
		/// <summary>
		/// Get or set the name of this student.
		/// </summary>
		public FullName Name
		{
			get { return this.name; }
			set { this.name = value; }
		}
		/// <summary>
		/// Get or set the description of this student.
		/// </summary>
		public string Description
		{
			get { return this.description; }
			set { this.description = value; }
		}
		/// <summary>
		/// Get or set the age ("unit?" !!!!) of this student.
		/// </summary>
		public uint? Age
		{
			get { return this.age; }
			set { this.age = value; }
		}
		/// <summary>
		/// Gets an array of additionalinformations of this student.
		/// Changes on this array causes no changes on the intern list!
		/// </summary>
		public AdditionalStudentInformation[] AdditionalInformations
		{
			get { return this.additionalInformations.ToArray(); }
		}
		/// <summary>
		/// Get the class of this student.
		/// The class can be changed via <see cref="SetStudentClass"/> or removed via <see cref="RemoveClass"/>.
		/// </summary>
		public StudentsClass StudentClass
		{
			get { return this.studentClass; }
			private set { this.studentClass = value; }
		}
		#endregion Public Properties

		public StudentPerson()
			: this(FullName.DEFAULT)
		{

		}

		public StudentPerson(FullName name, uint? age = null, string description = null, Sex gender = Sex.Unknown)
		{
			this.name = name;
			this.age = age;
			this.description = description;
			this.gender = gender;
			// copy the tempalte of additionalinformations to current class.
			this.additionalInformations = new List<AdditionalStudentInformation>(StudentPerson.additionalInformationTemplates);
		}

		public StudentPerson(string firstname, string lastname)
			: this(new FullName(firstname, lastname))
		{

		}

		public StudentPerson(string firstname, string lastname, uint? age = null, string description = "", Sex gender = Sex.Unknown)
			: this(new FullName(firstname, lastname), age: age, description: description, gender: gender)
		{

		}

		public void AddInformation(string id, object value, Type type, string description = "")
		{
			this.additionalInformations.Add(new AdditionalStudentInformation(id, value, type, description));
		}

		/// <summary>
		/// Set a new class of this student.
		/// Removes the student from the old class.
		/// </summary>
		/// <param name="studentClass"></param>
		public void SetStudentClass(StudentsClass studentClass)
		{
			if (studentClass == null) throw new ArgumentNullException("studentClass", "Parameter \"studentClass\" cannot be null!");
			if (this.studentClass != null)
			{
				this.studentClass.RemoveStudent(this);
				this.RemoveClass();
			}
			this.studentClass = studentClass;
		}

		/// <summary>
		/// This functions removes the class from this student.
		/// </summary>
		public void RemoveClass()
		{
			this.studentClass = null;
		}
		#region Struct's
		/// <summary>
		/// This struct represents a fullname.
		/// A fullname is the combination of first name and last name.
		/// </summary>
		public struct FullName
		{
			/// <summary>
			/// Get or set the first name.
			/// </summary>
			public string FirstName { get; set; }
			/// <summary>
			///  get or set the last name;
			/// </summary>
			public string LastName { get; set; }

			/// <summary>
			/// A default value.
			/// </summary>
			static readonly public FullName DEFAULT = new FullName("Unknown", "");

			/// <summary>
			/// Creates simple a new instance of fullname.
			/// </summary>
			/// <param name="firstname">The first name</param>
			/// <param name="lastname">The last name</param>
			public FullName(string firstname, string lastname)
				: this()
			{
				this.FirstName = firstname;
				this.LastName = lastname;
			}

			/// <summary>
			/// Return the plaintext value of this struct in the following format:
			/// #FIRSTNAME# #LASTNAME#
			/// if the lastname is empty:
			/// #FIRSTNAME#
			/// if firstname is empty:
			/// #LASTNAME#
			/// </summary>
			/// <returns>The plaintext if this struct-instance. The Format is descripted in the method information.</returns>
			public override string ToString()
			{
				// add a "ms" or "msr" clause..
				if (string.IsNullOrEmpty(this.FirstName)) return this.LastName;

				else
					if (string.IsNullOrEmpty(this.LastName)) return this.FirstName;
					else return this.FirstName + " " + this.LastName;
			}
		}

		#endregion Struct's

		#region Enums
		/// <summary>
		/// This enum contains all possible sex's =)
		/// </summary>
		public enum Sex
		{
			Unknown,	// Represents a default value ;)
			Male,		// boy
			Female		// girl
		}
		#endregion Enums
	}
}
