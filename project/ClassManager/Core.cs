﻿/* *** HEADER .\Core.cs ***
 *
 * # COPYRIGHT © Paul Holz - PeraSource - 2013
 * # ClassManager source file
 *
 * # Devoloped as homework for Mr. Jorcyk.
 *
 * # LICENSE / RIGHTS / OWNER:
 *		This code file is devoloped by Paul Holz. 
 *		If you use any code from here, please set a reference to this project!
 *
 * # CREATED:		8.1.2013
 * # BY:			Paul Holz
 * # SUMMARY:		This file contains the central object, which manages the program. The advantage is that the programm can easily contain a function like "minimize to system tray".
 *					In this way, the doesn't need program so much resources when it's minimized.
 * # LAST UPDATE:	21.1.2013
 * # BY:			Paul Holz
 * # SUMMARY:		Expanded INIT(), START(), fixed bug in loop foor filesystem rights.
 * 
 * # CHANGELOG
 *		# 10.1.2013
 *			- Added settings class & properties
 *			- added functionality to Core class
 *		# 21.1.2013:
 *			- Added RUNNAMEDFORM() - Runs a named form.
 *			- Expanded INIT(), START()...
 *			- Corrected FileSystemRights-loop in INIT()
 *				
 * # BUGTRACKER
 * 
 * # TODO
 *		- Make impressive!!
 *		- call & Write a "LOAD-DB"-func
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Windows.Forms;


namespace ClassManager
{
	static class Core
	{
		#region Public Static Properties
		/// <summary>
		/// Represents the full path to the programs executable.
		/// </summary>
		public static string ProgramPath { get; set; }

		/// <summary>
		/// Represents the full path to the program's directory.
		/// </summary>
		public static string ProgramDirectory { get; set; }

		public static Dictionary<string, Form> Forms { get; set; }

		#endregion Public Static Properties

		#region Private static Fields

		#endregion Private static Fields

		#region CCTOR
		/// <summary>
		/// The constructor intitializes the program's core class.
		/// Here will tasks like getting programlocation executed.
		/// </summary>
		static Core()
		{
			ProgramPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			ProgramDirectory = Path.GetDirectoryName(ProgramPath);

		}
		#endregion CCTOR

		#region Public Static Functions
		/// <summary>
		/// This function prepares the system for starting the program.
		/// Such as checking system for 
		/// </summary>
		static public void Init()
		{
			{
				bool writeAccess = false;
				AuthorizationRuleCollection accesRights = Directory.GetAccessControl(ProgramDirectory)
																   .GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));
				foreach (FileSystemAccessRule cur in accesRights)
				{

					if (cur.FileSystemRights.HasFlag(FileSystemRights.CreateFiles))
					{
						writeAccess = true;
						break;
					}
				}

				if (!writeAccess) throw new Exception("Sie benötigen Schreibzugriff auf den Programmordner, um diese Software ausführen zu können!");
			}

			Forms = new Dictionary<string, Form>();

			SQLite.Init(Path.Combine(Core.ProgramDirectory, Settings.DB_FILE_NAME));
			if (!SQLite.Exist())
			{
				SQLite.Create();
			}
		}

		public static void Start()
		{
			Forms.Add("MAIN", new FormMain());
			// beginne mit lesen v datensätzen aus datenbank.
			Application.Run(Forms["MAIN"]);
		}

		public static void RunNamedForm(string nameOfForm)
		{
			if (!Forms.ContainsKey(nameOfForm))
			{
				throw new ArgumentException("The specified form could'nt be found.", "nameOfForm: " + nameOfForm);
			}
			else
			{
				Form form = Forms[nameOfForm];
				if (form == null)
				{
					form = new Form();
				}
				form.Show();
			}
		}

		#endregion Public Static Functions

		#region Private Static functions

		#endregion Private Static functions

	}

	class Settings
	{
		#region Private Fields

		#endregion Private Fields

		#region Public Properties

		#endregion Public Properties

		#region Constant's (Public)
		public const string DB_FILE_NAME = "db.sqlite";
		#endregion Constant's (Public)

		public Settings()
		{





		}
	}
}
